from django.db import models

class ScheduleModels(models.Model):
    date = models.DateField()
    time = models.TimeField()
    name = models.CharField(max_length=100)
    place = models.CharField(max_length=100)
    category = models.CharField(max_length=100)
    #desc = models.TextField()

    def __str__(self):
        return self.name
    