from django.urls import re_path
from .views import *
# from .views import view_home
# from .views import view_contact
# from .views import view_register

#url for app
urlpatterns = [
    re_path(r'^home/', view_home, name='home'),
    re_path(r'^contact/', view_contact, name='contact'),
    re_path(r'^register/', view_register, name='register'),
    re_path(r'^schedule_form/', view_schedule_form, name='schedule_form'),
    re_path(r'^schedule_submit/', schedule_submit, name='schedule_submit'),
    re_path(r'^schedule_delete_all/', schedule_delete_all, name='schedule_delete_all'),
    re_path(r'^schedule/', view_schedule ,name='schedule'),
    re_path('',view_home,name = 'redirect'),
]
