from django.shortcuts import render
from django.http import HttpResponseRedirect
from datetime import datetime
from .models import ScheduleModels
from .forms import ScheduleForm


response = {}
# Create your views here.
# def view_redirect(request):
#     return render(request, 'home.html')

def view_home(request):
    #response = {'title' : web_name,'name' : person_name, 'time' : time_now }
    return render(request, 'home.html')  

def view_contact(request):
    return render(request, 'contact.html')

def view_register(request):
    return render(request, 'register.html')

def view_schedule(request):
    try :
        return render(request, 'schedule.html',{'schedule_list' : ScheduleModels.objects.all()})
    except Exception as e :    
        return render(request, 'schedule.html',{})

def view_schedule_form(request):
    try:
        return render(request, 'schedule_form.html', {'Form' : ScheduleForm})
    except Exception  as e:
        return render(request,'schedule_form.html')
    
def schedule_submit(request):
    form = ScheduleForm(request.POST or None)

    if (request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name'] if request.POST['name'] != "" else "Kegiatan tidak diketahui"
        response['date'] = request.POST['date'] if request.POST['date'] != "" else "Tanggal tidak diketahui"
        response['time'] = request.POST['time'] if request.POST['time'] != "" else "Jam tidak diketahui"
        response['place'] = request.POST['place'] if request.POST['place'] != "" else "Lokasi tidak diketahui"
        response['category'] = request.POST['category'] if request.POST['category'] != "" else "Kategori tidak diketahui"
        schedule = ScheduleModels(name=response['name'],date=response['date'],time=response['time'],
        place=response['place'], category=response['category'])
        
        schedule.save()
        return HttpResponseRedirect('/schedule/')
    else:
        return HttpResponseRedirect('/home/')


def schedule_delete_all(request):
    ScheduleModels.objects.all().delete()
    return HttpResponseRedirect('/schedule/')   