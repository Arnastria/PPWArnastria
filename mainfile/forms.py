from django import forms
import datetime

class ScheduleForm (forms.Form):

    error_messages = {
        'required' : 'Wajib diisi',
        'invalid' : 'Data tidak valid'
    }

    attrs_form = {
        'class': 'form-control'
        }

    attrs_date = {
        'class' : 'form-control',
        'type' : 'date'
    }

    attrs_time = {
        'class' : 'form-control',
        'type' : 'time'
    }

    name = forms.CharField(label='Nama Kegiatan', required=True,
                               widget=forms.TextInput(attrs=attrs_form))
    
    date = forms.DateField(label = 'Waktu', required=True,
                    widget = forms.DateInput(attrs = attrs_date))

    time = forms.TimeField(label = 'Jam', required=True,
                    widget = forms.TimeInput(attrs = attrs_time))

    place = forms.CharField(label='Lokasi', required=False,
                            widget=forms.TextInput(attrs=attrs_form))

    category = forms.CharField(label='Kategori', required=False,
                            widget=forms.TextInput(attrs=attrs_form))